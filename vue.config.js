module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
					@import "@/styles/variables/index.scss";
					@import "@/styles/mixins.scss";
					`
      }
    }
  }
};

import { shallowMount } from "@vue/test-utils";
import BaseButtons from "./BaseButtons";

describe("BaseButtons", () => {
  const wrapper = shallowMount(BaseButtons);

  it("renders the correct markup", () => {
    expect(wrapper.html()).toContain('<div class="buttons-hld"></div>');
  });
});

import { shallowMount } from "@vue/test-utils";
import BaseInput from "./BaseInput";

describe("BaseInput", () => {
  it("renders the correct markup", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        type: "Number"
      }
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders with all passed props", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        type: "text",
        value: "Test",
        placeholder: "test placeholder"
      }
    });

    expect(wrapper.find("input").element.value).toEqual("Test");
    expect(wrapper.find("input").attributes("type")).toEqual("text");
    expect(wrapper.find("input").attributes("placeholder")).toEqual(
      "test placeholder"
    );
  });

  it("emits event with value", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        type: "Number"
      }
    });

    wrapper.find("input").element.value = "123";
    wrapper.find("input").trigger("input");

    expect(wrapper.emitted("input")).toBeTruthy();
    expect(wrapper.emitted("input")[0][0]).toBe("123");
  });
});

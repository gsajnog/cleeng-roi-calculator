import Vue from "vue";
import Router from "vue-router";

import store from "@/state/store";

import Home from "@/views/Home.vue";
import Form from "@/views/Form.vue";
import Results from "@/views/Results.vue";
import ComplexResults from "@/views/ComplexResults.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/form/:step",
      name: "form",
      component: Form,
      beforeEnter(routeTo, routeFrom, next) {
        const nextFormStep = Number(routeTo.params.step);
        const stateStep = store.getters["currentFormStep"];

        if (nextFormStep !== stateStep) {
          return next({
            name: "home",
            redirect: "/"
          });
        }
        next();
      }
    },
    {
      path: "/results",
      name: "results",
      component: Results
    },
    {
      path: "/complex-results",
      name: "complex-results",
      component: ComplexResults
    },
    { path: "*", redirect: "/" }
  ]
});

import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const defaultState = {
  formStep: 1,
  hasAllDataForRequest: false,
  isFetchingData: false,
  isFetchError: false,
  streamQuality: [
    {
      key: "high",
      label: "High",
      default: true
    },
    {
      key: "mid",
      label: "Mid",
      default: false
    },
    {
      key: "entry",
      label: "Entry",
      default: false
    }
  ],
  formData: {
    quality: "mid",
    ticketSellAmount: "",
    ticketChargeAmount: ""
  },
  calculations: {}
};

export default new Vuex.Store({
  state: { ...defaultState },

  getters: {
    currentFormStep: state => state.formStep,
    streamQuality: state => state.streamQuality,
    formData: state => state.formData,
    formQuality: state => state.formData.quality,
    formTicketSell: state => state.formData.ticketSellAmount,
    formTicketCharge: state => state.formData.ticketChargeAmount,
    hasAllDataForRequest: state => state.hasAllDataForRequest,
    fetchBaseData: state => state.calculations.baseData,
    fetchComplexData: state => state.calculations.complexData,
    isFetchingData: state => state.isFetchingData,
    isFetchError: state => state.isFetchError
  },

  actions: {
    resetState({ commit }) {
      commit("resetState");
    },
    updateFormStep({ commit }, formStep) {
      commit("updateFormStep", formStep);
    },
    updateFormData({ commit }, payload) {
      commit("updateFormData", payload);
    },
    updateAllFormDataState({ commit }, payload) {
      commit("updateAllFormDataState", payload);
    },
    fetchData({ commit }, payload) {
      commit("fetchStart");

      return axios
        .post("http://localhost:3001/roi-results", { payload })
        .then(response => {
          const data = response.data;
          commit("fetchSuccess", data);
        })
        .catch(error => {
          commit("fetchFailed", error);
        });
    }
  },

  mutations: {
    ["resetState"](state) {
      Object.assign(state, defaultState);
    },
    ["updateFormStep"](state, formStep) {
      state.formStep = formStep;
    },
    ["updateFormData"](state, payload) {
      state.formData = {
        ...state.formData,
        ...payload
      };
    },
    ["updateAllDataState"](state, payload) {
      state.allDataForRequest = payload;
    },
    ["updateAllFormDataState"](state, payload) {
      state.hasAllDataForRequest = payload;
    },
    ["fetchData"](state, payload) {
      state.calculations = payload;
    },
    ["fetchStart"](state) {
      state.isFetchingData = true;
    },
    ["fetchSuccess"](state, data) {
      state.isFetchingData = false;
      state.isFetchError = false;
      state.calculations = data;
    },
    ["fetchFailed"](state) {
      state.isFetchingData = false;
      state.isFetchError = true;
    }
  }
});
